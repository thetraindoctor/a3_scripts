removeHeadgear _this;
removeGoggles _this;

civ_voices = ["Male01PER","Male02PER","Male03PER"];
civ_clothes = ["LOP_U_TAK_Civ_Fatigue_01",0.15,"LOP_U_TAK_Civ_Fatigue_02",0.15,"LOP_U_TAK_Civ_Fatigue_03",0.15,"LOP_U_TAK_Civ_Fatigue_04",0.15,"LOP_U_TAK_Civ_Fatigue_05",0.15,"LOP_U_TAK_Civ_Fatigue_06",0.15,"LOP_U_TAK_Civ_Fatigue_07",0.15,"LOP_U_TAK_Civ_Fatigue_08",0.15,"LOP_U_TAK_Civ_Fatigue_09",0.25,"LOP_U_TAK_Civ_Fatigue_10",0.25,"LOP_U_TAK_Civ_Fatigue_11",0.25,"LOP_U_TAK_Civ_Fatigue_12",0.25,"LOP_U_TAK_Civ_Fatigue_13",0.25,"LOP_U_TAK_Civ_Fatigue_14",0.25,"LOP_U_TAK_Civ_Fatigue_15",0.25,"LOP_U_TAK_Civ_Fatigue_16",0.25,"LOP_U_CHR_Policeman_01",0.02]; 
civ_faces = ["PersianHead_A3_01",0.25,"PersianHead_A3_02",0.25,"PersianHead_A3_03",0.25,"Mavros",0.25];
civ_hats = ["",0.5,"LOP_H_Pakol",0.8,"LOP_H_Turban",0.8];
civ_glasses = ["G_Squares",0.02,"G_Spectacles",0.02,"G_Spectacles_Tinted",0.02,"G_Aviator",0.02,"",11];

chosen_voice = selectRandom civ_voices;
chosen_clothes = selectRandomWeighted civ_clothes;
chosen_face = selectRandomWeighted civ_faces;
if(chosen_clothes == "LOP_U_CHR_Policeman_01") then {
	chosen_hat = "LOP_H_Policeman_cap";
} else {chosen_hat = selectRandomWeighted civ_hats;};
chosen_glasses = selectRandomWeighted civ_glasses;

if (isServer || isDedicated) then {[_this, chosen_voice] remoteExec ["setSpeaker", 0, _this]}
else {_this setSpeaker chosen_voice};
_this forceAddUniform chosen_clothes;
_this setFace chosen_face;
_this addHeadgear chosen_hat;
_this addGoggles chosen_glasses;