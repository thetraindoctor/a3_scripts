removeHeadgear _this;
removeGoggles _this;

civ_voices = ["RHS_Male01CZ","RHS_Male02CZ","RHS_Male03CZ","RHS_Male04CZ","RHS_Male05CZ"];
civ_clothes = ["LOP_U_CHR_Villager_01",0.15,"LOP_U_CHR_Villager_02",0.15,"LOP_U_CHR_Villager_03",0.15,"LOP_U_CHR_Villager_04",0.15,"LOP_U_CHR_Worker_01",0.25,"LOP_U_CHR_Worker_02",0.25,"LOP_U_CHR_Worker_03",0.25,"LOP_U_CHR_Worker_04",0.25,"LOP_U_CHR_Citizen_01",0.25,"LOP_U_CHR_Citizen_02",0.25,"LOP_U_CHR_Citizen_03",0.25,"LOP_U_CHR_Citizen_04",0.25,"LOP_U_CHR_Profiteer_01",0.25,"LOP_U_CHR_Profiteer_02",0.25,"LOP_U_CHR_Profiteer_03",0.25,"LOP_U_CHR_Profiteer_04",0.25,"LOP_U_CHR_Woodlander_01",0.2,"LOP_U_CHR_Woodlander_02",0.2,"LOP_U_CHR_Woodlander_03",0.2,"LOP_U_CHR_Woodlander_04",0.2];
civ_faces = ["WhiteHead_01",0.25,"WhiteHead_03",0.25,"WhiteHead_18",0.25,"WhiteHead_07",0.25,"WhiteHead_08",0.25,"WhiteHead_16",0.25,"WhiteHead_23",0.25,"WhiteHead_21",0.25,"WhiteHead_13",0.25];
civ_hats = ["LOP_H_Ushanka",0.25,"LOP_H_Villager_cap",0.25,"LOP_H_Worker_cap",0.25,"H_Cap_tan",0.25,"H_Cap_blk",0.25,"H_Cap_red",0.25,"H_Cap_grn",0.25,"H_Cap_blu",0.25,"H_Beret_blk",0.25,"H_Beret_red",0.25,"H_Beret_grn",0.25,"",11];
civ_glasses = ["G_Squares",0.25,"G_Spectacles",0.25,"G_Spectacles_Tinted",0.25,"G_Aviator",0.05,"",11];

chosen_voice = selectRandom civ_voices;
chosen_clothes = selectRandomWeighted civ_clothes;
chosen_face = selectRandomWeighted civ_faces;
chosen_hat = selectRandomWeighted civ_hats;
chosen_glasses = selectRandomWeighted civ_glasses;

if (isServer || isDedicated) then {[_this, chosen_voice] remoteExec ["setSpeaker", 0, _this]}
else {_this setSpeaker chosen_voice};
_this forceAddUniform chosen_clothes;
_this setFace chosen_face;
_this addHeadgear chosen_hat;
_this addGoggles chosen_glasses;