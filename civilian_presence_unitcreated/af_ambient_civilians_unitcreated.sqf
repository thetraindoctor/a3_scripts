removeHeadgear _this;
removeGoggles _this;

civ_voices = ["Male01FRE","Male02FRE","Male03FRE","Male01ENGFRE","Male02ENGFRE"];
civ_clothes = ["LOP_U_AFR_Civ_01",0.15,"LOP_U_AFR_Civ_02",0.15,"LOP_U_AFR_Civ_03",0.15,"LOP_U_AFR_Civ_04",0.15,"LOP_U_AFR_Civ_05",0.25,"LOP_U_AFR_Civ_06",0.25,"LOP_U_AFR_Civ_01S",0.15,"LOP_U_AFR_Civ_02S",0.15,"LOP_U_AFR_Civ_03S",0.15,"LOP_U_AFR_Civ_04S",0.15,"LOP_U_AFR_Civ_05S",0.25,"LOP_U_AFR_Civ_06S",0.25];
civ_faces = ["TanoanHead_A3_01",0.25,"TanoanHead_A3_02",0.25,"TanoanHead_A3_03",0.25,"TanoanHead_A3_04",0.25,"TanoanHead_A3_05",0.25,"TanoanHead_A3_06",0.25,"TanoanHead_A3_07",0.25,"TanoanHead_A3_08",0.25,"Barklem",0.25,"AfricanHead_01",0.25,"AfricanHead_02",0.25,"AfricanHead_03",0.25];
civ_hats = ["",0.5,"PO_H_Bandanna_blu",0.3,"PO_H_Bandanna_gry",0.3,"PO_H_Bandanna_sand",0.3,"PO_H_Booniehat_khk",0.2,"PO_H_Booniehat_oli",0.2,"PO_H_Booniehat_grn",0.2,"PO_H_Booniehat_tan",0.2,"PO_H_Booniehat_dirty",0.2,"PO_H_Cap_blu",0.1,"PO_H_Cap_red",0.1,"PO_H_Cap_grn",0.1,"PO_H_Cap_oli",0.1,"",2];
civ_glasses = ["G_Squares",0.25,"G_Spectacles",0.25,"G_Spectacles_Tinted",0.25,"G_Aviator",0.05,"",11];

chosen_voice = selectRandom civ_voices;
chosen_clothes = selectRandomWeighted civ_clothes;
chosen_face = selectRandomWeighted civ_faces;
chosen_hat = selectRandomWeighted civ_hats;
chosen_glasses = selectRandomWeighted civ_glasses;

if (isServer || isDedicated) then {[_this, chosen_voice] remoteExec ["setSpeaker", 0, _this]}
else {_this setSpeaker chosen_voice};
_this forceAddUniform chosen_clothes;
_this setFace chosen_face;
_this addHeadgear chosen_hat;
_this addGoggles chosen_glasses;