_this_az = getDir this;
_distance = random[-3,-6,-10];
boomstr = format ["Boom goes %1",player];
this = createTrigger ["EmptyDetector", getPos this, true];
this setTriggerArea [1, distance, _this_az, false];
this setTriggerActivation ["ANYPLAYER", "PRESENT", true];
this setTriggerTimeout [0,2,3,true];
this setTriggerStatements ["this", "_mine setDamage 1; hint boomstr",""];