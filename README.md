# Welcome
This repository is a collection of all the random assorted (some of them not so random or assorted) that I have created for many different uses.
Most of these scripts are extremely niche, and sometimes (rarely) will have more than just one variant for various different applications.
Scripts which can and cannot be used out of the box will be marked in the wiki accordingly.
If you make any clever additions to my scripts, please make a merge request! I would love to see my random assorted that was made into something useful!  
More information is avaliable in the wiki [here](https://gitlab.com/thetraindoctor/a3_scripts/wikis/home).